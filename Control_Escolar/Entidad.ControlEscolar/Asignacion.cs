﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidad.ControlEscolar
{
    public class Asignacion
    {
        private int _idasignacion;
        private string _maestroa;
        private string _materiaa;
        private string _fknombregrupo;

        public int Idasignacion { get => _idasignacion; set => _idasignacion = value; }
        public string Maestroa { get => _maestroa; set => _maestroa = value; }
        public string Materiaa { get => _materiaa; set => _materiaa = value; }
        public string Fknombregrupo { get => _fknombregrupo; set => _fknombregrupo = value; }
    }
}
