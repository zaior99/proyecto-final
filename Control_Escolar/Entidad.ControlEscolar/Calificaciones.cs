﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidad.ControlEscolar
{
    public class Calificaciones
    {
        private int _idparcial1;
        private string _grupo;
        private string _materia;
        private string _nombrealumno;
        private int _parcial1;
        private int _parcial2;
        private int _parcial3;
        private int _parcial4;

        public int Idparcial1 { get => _idparcial1; set => _idparcial1 = value; }
        public string Grupo { get => _grupo; set => _grupo = value; }
        public string Materia { get => _materia; set => _materia = value; }
        public string Nombrealumno { get => _nombrealumno; set => _nombrealumno = value; }
        public int Parcial1 { get => _parcial1; set => _parcial1 = value; }
        public int Parcial2 { get => _parcial2; set => _parcial2 = value; }
        public int Parcial3 { get => _parcial3; set => _parcial3 = value; }
        public int Parcial4 { get => _parcial4; set => _parcial4 = value; }
    }
}
