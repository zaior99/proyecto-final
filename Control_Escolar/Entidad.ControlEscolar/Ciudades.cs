﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidad.ControlEscolar
{
    public class Ciudades
    {
        private int _codigocuidad;
        private string _nombre;
        private string _fkcodigoestado;

        public int Codigocuidad { get => _codigocuidad; set => _codigocuidad = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string Fkcodigoestado { get => _fkcodigoestado; set => _fkcodigoestado = value; }
    }
}
