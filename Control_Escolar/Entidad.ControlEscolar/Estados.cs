﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidad.ControlEscolar
{
    public class Estados
    {
        private string _codigoestado;
        private string _nombre;

        public string Codigoestado { get => _codigoestado; set => _codigoestado = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
    }
}
