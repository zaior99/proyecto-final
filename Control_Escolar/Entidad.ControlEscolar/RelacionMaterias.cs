﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidad.ControlEscolar
{
    public class RelacionMaterias
    {
        private int _idrelacion;
        private string _materia;
        private string _antesesora;
        private string _sucesora;

        public int Idrelacion { get => _idrelacion; set => _idrelacion = value; }
        public string Materia { get => _materia; set => _materia = value; }
        public string Antesesora { get => _antesesora; set => _antesesora = value; }
        public string Sucesora { get => _sucesora; set => _sucesora = value; }
    }
}
