﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidad.ControlEscolar
{
    public class GrupoAlumnos
    {
        private int _idgrupoa;
        private string _nombrealumno;
        private string _fknombregrupo;

        public int Idgrupoa { get => _idgrupoa; set => _idgrupoa = value; }
        public string Nombrealumno { get => _nombrealumno; set => _nombrealumno = value; }
        public string Fknombregrupo { get => _fknombregrupo; set => _fknombregrupo = value; }
    }
}
