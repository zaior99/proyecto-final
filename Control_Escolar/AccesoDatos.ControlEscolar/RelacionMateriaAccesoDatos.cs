﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class RelacionMateriaAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public RelacionMateriaAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresas", 3306);
        }
        public void Guardar(RelacionMaterias rm)
        {
            if (rm.Idrelacion == 0)
            {
                string consulta = string.Format("Insert into materiarelacion values(null,'{0}','{1}','{2}')",
                rm.Materia, rm.Antesesora, rm.Sucesora);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update materiarelacion set materia ='{0}', " +
                "antes ='{1}',despues = '{2}' where idmateriarelacion = {3}",
                rm.Materia, rm.Antesesora, rm.Sucesora, rm.Materia);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void Eliminar(int idRelacion)
        {
            string consulta = string.Format("Delete from materiarelacion where idmateriarelacion = {0}", idRelacion);
            conexion.EjecutarConsulta(consulta);
        }
        public List<RelacionMaterias> GetRelacionMaterias(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listRelacionMateria = new List<RelacionMaterias>();
            var ds = new DataSet();
            string consulta = "Select * from materiarelacion where antes like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "materiarelacion");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var relacion = new RelacionMaterias
                {
                    Idrelacion = Convert.ToInt32(row["idmateriarelacion"]),
                    Materia = row["materia"].ToString(),                 
                    Antesesora = row["antes"].ToString(),
                    Sucesora = row["despues"].ToString(),

                };
                listRelacionMateria.Add(relacion);
            }
            return listRelacionMateria;
        }

    }
}
