﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class CalificacionAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public CalificacionAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresas", 3306);
        }
        public void Guardar(Calificaciones calificaciones)
        {
            if (calificaciones.Idparcial1 == 0)
            {
                string consulta = string.Format("Insert into calificaciones values(null,'{0}','{1}','{2}','{3}','{4}','{5}','{6}')",
                calificaciones.Grupo, calificaciones.Materia, calificaciones.Nombrealumno, calificaciones.Parcial1,calificaciones.Parcial2, calificaciones.Parcial3, calificaciones.Parcial4);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update calificaciones set grupo ='{0}', materia ='{1}', nombrealumno ='{2}', parcial1 ='{3}', parcial2 ='{4}', parcial3 ='{5}', parcial4 ='{6}' where idparcial1 = '{7}'",
                calificaciones.Grupo, calificaciones.Materia, calificaciones.Nombrealumno, calificaciones.Parcial1,calificaciones.Parcial2, calificaciones.Parcial3, calificaciones.Parcial4, calificaciones.Idparcial1);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void Eliminar(int idParcial1)
        {
            string consulta = string.Format("Delete from calificaciones where idparcial1 = {0}", idParcial1);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Calificaciones> GetCalificaciones(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listCalificacion = new List<Calificaciones>();
            var ds = new DataSet();
            string consulta = "Select * from calificaciones where grupo like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "calificaciones");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var calificacion = new Calificaciones
                {
                    Idparcial1 = Convert.ToInt32(row["idparcial1"]),
                    Grupo = row["grupo"].ToString(),
                    Materia = row["materia"].ToString(),
                    Nombrealumno = row["nombrealumno"].ToString(),
                    Parcial1 = Convert.ToInt32(row["parcial1"]),
                    Parcial2 = Convert.ToInt32(row["parcial2"]),
                    Parcial3 = Convert.ToInt32(row["parcial3"]),
                    Parcial4 = Convert.ToInt32(row["parcial4"]),


                };
                listCalificacion.Add(calificacion);
            }
            return listCalificacion;
        }
    }
}
