﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class EstadoAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public EstadoAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresas", 3306);
        }
        
        public List<Estados> GetEstados(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listEstados = new List<Estados>();
            var ds = new DataSet();
            string consulta = "Select * from Estados where codigoestado like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "Estados");

            var dt = new DataTable();
            dt = ds.Tables[0];


            foreach (DataRow row in dt.Rows)
            {
                var estados = new Estados
                {           
                    Codigoestado = row["codigoestado"].ToString(),
                    Nombre= row["nombre"].ToString(),
 
                };
                listEstados.Add(estados);
            }
            return listEstados;
        }
        
    }
}
