﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class EscuelaAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public EscuelaAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresas", 3306);
        }
        public void Guardar(Escuela escuela)
        {
            if (escuela.Idescuela == 0)
            {
                string consulta = string.Format("Insert into escuela values(null,'{1}','{2}','{3}','{4}','{5}','{6}','{7}')",
                escuela.Nombre, escuela.RFC, escuela.Domicilio, escuela.Telefono, escuela.Correo, escuela.Paginaweb, escuela.Director, escuela.Logo);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update escuela set nombre ='{0}', RFC ='{1}', " +
                "domicilio ='{2}', telefono ='{3}', correo ='{4}', paginaweb ='{5}', director ='{6}'," +
                " logo ='{7}' where idescuela = {8}",
                escuela.Nombre, escuela.RFC, escuela.Domicilio, escuela.Telefono, escuela.Correo, escuela.Paginaweb, escuela.Director, escuela.Logo,             
                escuela.Idescuela);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void Eliminar(int idEscuela)
        {

            string consulta = string.Format("Delete from escuela where idescuela = {0}", idEscuela);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Escuela> GetEscuela(string filtro)
        {
            List<Usuario> listUsuario = new List<Usuario>();
            var listEscuela = new List<Escuela>();
            var ds = new DataSet();
            string consulta = "Select * from escuela where nombre like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "escuela");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var escuela = new Escuela
                {
                    Idescuela = Convert.ToInt32(row["idescuela"]),
                    Nombre = row["nombre"].ToString(),
                    RFC = row["RFC"].ToString(),
                    Domicilio = row["domicilio"].ToString(),
                    Telefono = row["telefono"].ToString(),
                    Correo = row["correo"].ToString(),
                    Paginaweb = row["paginaweb"].ToString(),
                    Director = row["director"].ToString(),
                    Logo = row["logo"].ToString(),

                };
                listEscuela.Add(escuela);

            }

            return listEscuela;
        }
        public DataSet Ver()
        {
            DataSet ds = new DataSet();
            string consulta = string.Format("Select * from escuela");
            ds = conexion.ObtenerDatos(consulta, "escuela");
            return ds;                 
        }
        
    }
}
