﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class GrupoAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public GrupoAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresas", 3306);
        }
        public void Guardar(Grupo grupo)
        {
            if (grupo.Nombregrupo != " ")
            {
                string consulta = string.Format("Insert into grupo values('{0}')",
                grupo.Nombregrupo);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update grupo set nombregrupo ='{0}'",
                grupo.Nombregrupo);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void Eliminar(string idGrupo)
        {
            string consulta = string.Format("Delete from grupo where nombregrupo = '{0}'", idGrupo);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Grupo> GetGrupo(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listGrupo = new List<Grupo>();
            var ds = new DataSet();
            string consulta = "Select * from grupo where nombregrupo like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "grupo");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var grupo = new Grupo
                {
                    
                    Nombregrupo = row["nombregrupo"].ToString(),
                };
                listGrupo.Add(grupo);
            }
            return listGrupo;
        }
    }
}
