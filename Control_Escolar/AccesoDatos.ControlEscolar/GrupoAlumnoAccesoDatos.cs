﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class GrupoAlumnoAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public GrupoAlumnoAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresas", 3306);
        }
        public void Guardar(GrupoAlumnos grupoAlumnos)
        {
            if (grupoAlumnos.Idgrupoa == 0)
            {
                string consulta = string.Format("Insert into grupoalumno values(null,'{0}','{1}')",
                grupoAlumnos.Nombrealumno, grupoAlumnos.Fknombregrupo);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update grupoalumno set nombrealumno ='{0}', fknombregrupo ='{1}' where idgrupoa = '{2}'",
                grupoAlumnos.Nombrealumno, grupoAlumnos.Fknombregrupo, grupoAlumnos.Idgrupoa);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void Eliminar(int idGrupoAlumno)
        {
            string consulta = string.Format("Delete from grupoalumno where idgrupoa = {0}", idGrupoAlumno);
            conexion.EjecutarConsulta(consulta);
        }
        public List<GrupoAlumnos> GetGrupoAlumnos(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listGrupoAlumnos = new List<GrupoAlumnos>();
            var ds = new DataSet();
            string consulta = "Select * from grupoalumno where fknombregrupo like '%" + filtro + "%'";
            //string consulta = "select idgrupoa, nombrealumno, nombregrupo from grupoalumno,grupo where grupoalumno.fknombregrupo = grupo.idgrupo";
            ds = conexion.ObtenerDatos(consulta, "grupoalumno");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var grupoalumnos = new GrupoAlumnos
                {
                    Idgrupoa = Convert.ToInt32(row["idgrupoa"]),                 
                    Nombrealumno = row["nombrealumno"].ToString(),
                    Fknombregrupo = row["fknombregrupo"].ToString(),


                };
                listGrupoAlumnos.Add(grupoalumnos);
            }
            return listGrupoAlumnos;
        }
    }
}
