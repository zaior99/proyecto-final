﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class MateriaAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public MateriaAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresas", 3306);
        }
        public void Guardar(Materias materias)
        {
            if (materias.Idmateria == 0)
            {
                string consulta = string.Format("Insert into materia values(null,'{0}','{1}','{2}','{3}')",
                materias.Nombremateria, materias.Hrsteoria, materias.Hrspractica,materias.Creditos);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update materia set nombremateria ='{0}', hrsteoria ='{1}', hrspractica ='{2}', creditos ='{3}' where idmateria = {4}",
                materias.Nombremateria, materias.Hrsteoria, materias.Hrspractica,materias.Creditos, materias.Idmateria);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void Eliminar(int idMateria)
        {
            string consulta = string.Format("Delete from materia where idmateria = {0}", idMateria);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Materias> GetMateria(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listMateria = new List<Materias>();
            var ds = new DataSet();
            string consulta = "Select * from materia where nombremateria like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "materia");

            var dt = new DataTable();
            dt = ds.Tables[0];


            foreach (DataRow row in dt.Rows)
            {
                var materia = new Materias
                {
                    Idmateria = Convert.ToInt32(row["idmateria"]),
                    Nombremateria = row["nombremateria"].ToString(),
                    Hrsteoria = Convert.ToInt32( row["hrsteoria"]),
                    Hrspractica = Convert.ToInt32( row["hrspractica"]),
                    Creditos = Convert.ToInt32(row["creditos"]),
                    
                };
                listMateria.Add(materia);
            }
            return listMateria;
        }
    }

}

