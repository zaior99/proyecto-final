﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class EstudioAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public EstudioAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresas", 3306);
        }
        public void Guardar(Estudios estudios)
        {
            if (estudios.Idestudio == 0)
            {
                string consulta = string.Format("Insert into estudios values(null,'{0}','{1}','{2}')",
                estudios.Estudio, estudios.Nombre, estudios.Documento);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update estudios set estudio ='{0}', fkidprofesor ='{1}',documento = '{2}' where idestudio = '{3}'",
                estudios.Estudio, estudios.Nombre, estudios.Documento,estudios.Idestudio);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void Eliminar(int idEstudio)
        {
            string consulta = string.Format("Delete from estudios where idestudio = {0}", idEstudio);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Estudios> GetEstudios(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listEstudio = new List<Estudios>();
            var ds = new DataSet();
            string consulta = "Select idestudio, nombre, estudio, documento from estudios, profesor where estudios.fkidprofesor = profesor.idprofesor";
            ds = conexion.ObtenerDatos(consulta, "estudios");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var estudio = new Estudios
                {
                    Idestudio = Convert.ToInt32(row["idestudio"]),
                    Nombre = row["nombre"].ToString(),
                    Estudio = row["estudio"].ToString(),                   
                    Documento = row["documento"].ToString(),
                    
                };
                listEstudio.Add(estudio);
            }
            return listEstudio;
        }
    }
}
