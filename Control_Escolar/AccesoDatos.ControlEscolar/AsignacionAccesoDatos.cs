﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class AsignacionAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public AsignacionAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresas", 3306);
        }
        public void Guardar(Asignacion asignacion)
        {
            if (asignacion.Idasignacion == 0)
            {
                string consulta = string.Format("Insert into asignacion values(null,'{0}','{1}','{2}')",
                asignacion.Maestroa, asignacion.Materiaa, asignacion.Fknombregrupo);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update asignacion set maestroa ='{0}', materiaa ='{1}', fknombregrupo ='{2}' where idasignacion = '{3}'",
                asignacion.Maestroa, asignacion.Materiaa, asignacion.Fknombregrupo, asignacion.Idasignacion);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void Eliminar(int idAsignacion)
        {
            string consulta = string.Format("Delete from asignacion where idasignacion = {0}", idAsignacion);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Asignacion> GetAsignacions(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listAsignacion = new List<Asignacion>();
            var ds = new DataSet();
            string consulta = "Select * from asignacion where fknombregrupo like '%" + filtro + "%'";
            //string consulta = "Select idasignacion, maestroa, materiaa, nombregrupo from asignacion,grupo where asignacion.fkidgrupo = grupo.idgrupo";
            ds = conexion.ObtenerDatos(consulta, "asignacion");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var asigna = new Asignacion
                {
                    Idasignacion = Convert.ToInt32(row["idasignacion"]),
                    Maestroa = row["maestroa"].ToString(),
                    Materiaa = row["materiaa"].ToString(),
                    Fknombregrupo = row["fknombregrupo"].ToString(),

                };
                listAsignacion.Add(asigna);
            }
            return listAsignacion;
        }
    }
}
