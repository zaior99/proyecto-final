﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using AccesoDatos.ControlEscolar;
using System.Data;

namespace LogicaNegocio.ControlEscolar
{
    public class ProfesorManejador
    {
        private ProfesorAccesoDatos _profesorAccesoDatos = new ProfesorAccesoDatos();

        public void Guardar(Profesores profesor)
        {
            _profesorAccesoDatos.Guardar(profesor);

        }
        public void Eliminar(int idProfesor)
        {
            _profesorAccesoDatos.Eliminar(idProfesor);
        }
        public List<Profesores> GetProfesor(string filtro)
        {
            var listProfesor = _profesorAccesoDatos.GetProfesor(filtro);

            return listProfesor;
        }
        public DataSet Nocontrol(string i)
        {
            return _profesorAccesoDatos.Ncontrol(i);
        }

    }
}
