﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class GrupoAlumnoManejador
    {
        private GrupoAlumnoAccesoDatos _grupoAlumnoAccesoDatos = new GrupoAlumnoAccesoDatos();

        public void Guardar(GrupoAlumnos grupoAlumnos)
        {
            _grupoAlumnoAccesoDatos.Guardar(grupoAlumnos);

        }
        public void Eliminar(int idgrupoAlumnos)
        {
            _grupoAlumnoAccesoDatos.Eliminar(idgrupoAlumnos);
        }
        public List<GrupoAlumnos> GetGrupoAlumnos(string filtro)
        {
            var listGrupoAlumnos = _grupoAlumnoAccesoDatos.GetGrupoAlumnos(filtro);

            return listGrupoAlumnos;
        }

    }
}
