﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class AlumnoManejador
    {
        private AlumnoAccesoDatos _alumnoAccesoDatos = new AlumnoAccesoDatos();

        public void Guardar(Alumnos alumno)
        {
            _alumnoAccesoDatos.Guardar(alumno);

        }
        public void Eliminar(int idAlumno)
        {
            _alumnoAccesoDatos.Eliminar(idAlumno);
        }
        public List<Alumnos> GetAlumno(string filtro)
        {
            var listAlumno = _alumnoAccesoDatos.GetAlumno(filtro);

            return listAlumno;
        }
    }
}
