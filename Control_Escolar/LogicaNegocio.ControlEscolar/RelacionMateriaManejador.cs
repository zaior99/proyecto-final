﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public  class RelacionMateriaManejador
    {
        private RelacionMateriaAccesoDatos _relacionMateriaAccesoDatos = new RelacionMateriaAccesoDatos();

        public void Guardar(RelacionMaterias relacionMaterias)
        {
            _relacionMateriaAccesoDatos.Guardar(relacionMaterias);

        }
        public void Eliminar(int idRelacion)
        {
            _relacionMateriaAccesoDatos.Eliminar(idRelacion);
        }
        public List<RelacionMaterias> GetRelacionMaterias(string filtro)
        {
            var listRelacionMateria = _relacionMateriaAccesoDatos.GetRelacionMaterias(filtro);

            return listRelacionMateria;
        }
    }
}
