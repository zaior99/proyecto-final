﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class AsignacionManejador
    {
        private AsignacionAccesoDatos _asignacionAccesoDatos = new AsignacionAccesoDatos();

        public void Guardar(Asignacion asignacion)
        {
            _asignacionAccesoDatos.Guardar(asignacion);

        }
        public void Eliminar(int idAsignacion)
        {
            _asignacionAccesoDatos.Eliminar(idAsignacion);
        }
        public List<Asignacion> GetAsignacions(string filtro)
        {
            var listAsignacion = _asignacionAccesoDatos.GetAsignacions(filtro);

            return listAsignacion;
        }
    }
}
