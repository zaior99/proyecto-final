﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos.ControlEscolar;
using Entidad.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class EstadoManejador
    {
        private EstadoAccesoDatos _estadosAccesoDatos = new EstadoAccesoDatos();

        public List<Estados> GetEstados(string filtro)
        {
            var listEstados = _estadosAccesoDatos.GetEstados(filtro);

            return listEstados;
        }
    }
}
