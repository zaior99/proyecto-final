﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class EstudioManejador
    {
        private EstudioAccesoDatos _estudioAccesoDatos = new EstudioAccesoDatos();

        public void Guardar(Estudios estudio)
        {
            _estudioAccesoDatos.Guardar(estudio);

        }
        public void Eliminar(int idEstudio)
        {
            _estudioAccesoDatos.Eliminar(idEstudio);
        }
        public List<Estudios> GetEstudio(string filtro)
        {
            var listEstudio = _estudioAccesoDatos.GetEstudios(filtro);

            return listEstudio;
        }
    }
}
