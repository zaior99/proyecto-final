﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class GrupoManejador
    {
        private GrupoAccesoDatos _grupoAccesoDatos = new GrupoAccesoDatos();

        public void Guardar(Grupo grupo)
        {
            _grupoAccesoDatos.Guardar(grupo);

        }
        public void Eliminar(string idgrupo)
        {
            _grupoAccesoDatos.Eliminar(idgrupo);
        }
        public List<Grupo> GetGrupo(string filtro)
        {
            var listGrupo = _grupoAccesoDatos.GetGrupo(filtro);

            return listGrupo;
        }
    }
}
