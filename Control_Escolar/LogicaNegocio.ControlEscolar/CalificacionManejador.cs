﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class CalificacionManejador
    {
        private CalificacionAccesoDatos _calificacionAccesoDatos = new CalificacionAccesoDatos();

        public void Guardar(Calificaciones calificaciones)
        {
            _calificacionAccesoDatos.Guardar(calificaciones);

        }
        public void Eliminar(int idParcial1)
        {
            _calificacionAccesoDatos.Eliminar(idParcial1);
        }
        public List<Calificaciones> GetCalificaciones(string filtro)
        {
            var listCalificacion = _calificacionAccesoDatos.GetCalificaciones(filtro);

            return listCalificacion;
        }
    }
}
