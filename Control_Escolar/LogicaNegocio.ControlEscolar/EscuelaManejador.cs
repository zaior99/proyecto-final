﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using AccesoDatos.ControlEscolar;
using System.Data;

namespace LogicaNegocio.ControlEscolar
{
    public class EscuelaManejador
    {
        private EscuelaAccesoDatos _escuelaAccesoDatos = new EscuelaAccesoDatos();

        public void Guardar(Escuela escuela)
        {
            _escuelaAccesoDatos.Guardar(escuela);

        }
        public void Eliminar(int idEscuela)
        {
            _escuelaAccesoDatos.Eliminar(idEscuela);
        }
        public List<Escuela> GetEscuela(string filtro)
        {
            var listEscuela = _escuelaAccesoDatos.GetEscuela(filtro);

            return listEscuela;
        }
        
        public DataSet Ver()
        {
            DataSet ds = _escuelaAccesoDatos.Ver();
            return ds;       
        }
        

    }
}
