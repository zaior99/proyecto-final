﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidad.ControlEscolar;
using LogicaNegocio.ControlEscolar;

namespace Control_Escolar
{
    public partial class AsignacionMaestro : Form
    {
        private AsignacionManejador _asignacionManejador;
        private ProfesorManejador _profesorManejador;
        private MateriaManejador _materiaManejador;
        private GrupoAlumnoManejador _grupoAlumnoManejador;
        private GrupoManejador _grupoManejador;

        public AsignacionMaestro()
        {
            InitializeComponent();
            _asignacionManejador = new AsignacionManejador();
            _profesorManejador = new ProfesorManejador();
            _materiaManejador = new MateriaManejador();
            _grupoAlumnoManejador = new GrupoAlumnoManejador();
            _grupoManejador = new GrupoManejador();
            
        }

        private void AsignacionMaestro_Load(object sender, EventArgs e)
        {
            buscarasignacion("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            buscarasignacion(txtBuscar.Text);
        }
        private void buscarasignacion(string filtro)
        {

            dtgAsignacion.DataSource = _asignacionManejador.GetAsignacions(filtro);

        }
        private void TraerProfesor(string filtro)
        {
            cmbMaestro.DataSource = _profesorManejador.GetProfesor(filtro);
            cmbMaestro.DisplayMember = "nombre";
            cmbMaestro.ValueMember = "idprofesor";

        }
        private void TraerMateria(string filtro)
        {
            cmbMateria.DataSource = _materiaManejador.GetMateria(filtro);
            cmbMateria.DisplayMember = "nombremateria";
            cmbMateria.ValueMember = "idmateria";

        }
        private void TraerGrupo(string filtro)
        {
            cmbGrupo.DataSource = _grupoManejador.GetGrupo(filtro);
            cmbGrupo.DisplayMember = "nombregrupo";
            //cmbGrupo.ValueMember = "idgrupo";

        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            cmbMaestro.Enabled = activar;
            cmbMateria.Enabled = activar;
            cmbGrupo.Enabled = activar;

        }
        private void LimpiarCuadros()
        {
            cmbMaestro.Text = "";
            cmbMateria.Text = "";
            cmbGrupo.Text = "";

        }
        private void GuardarAsignacion()
        {
            _asignacionManejador.Guardar(new Asignacion
            {
                Idasignacion = Convert.ToInt32(lblId.Text),
                Maestroa = cmbMaestro.Text,
                Materiaa = cmbMateria.Text,
                Fknombregrupo = cmbGrupo.Text,
            });
        }
        private void EliminarAsignacion()
        {
            var Idasignacion = dtgAsignacion.CurrentRow.Cells["idasignacion"].Value;
            _asignacionManejador.Eliminar(Convert.ToInt32(Idasignacion));
        }
        private void ModificarAsignacion()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);

            lblId.Text = dtgAsignacion.CurrentRow.Cells["idasignacion"].Value.ToString();
            cmbMaestro.Text = dtgAsignacion.CurrentRow.Cells["maestroa"].Value.ToString();
            cmbMateria.Text = dtgAsignacion.CurrentRow.Cells["materiaa"].Value.ToString();
            cmbGrupo.Text = dtgAsignacion.CurrentRow.Cells["grupo"].Value.ToString();

        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
            cmbMaestro.Focus();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            try
            {
                GuardarAsignacion();
                LimpiarCuadros();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar este registro", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarAsignacion();
                    buscarasignacion("");

                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }

            }
        }

        private void dtgAsignacion_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dtgAsignacion_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarAsignacion();
                buscarasignacion("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void cmbMaestro_Click(object sender, EventArgs e)
        {
            TraerProfesor("");
        }

        private void cmbMateria_Click(object sender, EventArgs e)
        {
            TraerMateria("");
        }

        private void cmbGrupo_Click(object sender, EventArgs e)
        {
            TraerGrupo("");
        }
    }
}
