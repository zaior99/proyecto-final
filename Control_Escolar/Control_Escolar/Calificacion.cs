﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidad.ControlEscolar;
using LogicaNegocio.ControlEscolar;
using Microsoft.Office.Interop.Excel;

namespace Control_Escolar
{
    public partial class Calificacion : Form
    {
        private CalificacionManejador _calificacionManejador;
        private AsignacionManejador _asignacionManejador;      
        private GrupoAlumnoManejador _grupoAlumnoManejador;
        private GrupoManejador _grupoManejador;

        public Calificacion()
        {
            InitializeComponent();
            _calificacionManejador = new CalificacionManejador();
            _asignacionManejador = new AsignacionManejador();
            _grupoAlumnoManejador = new GrupoAlumnoManejador();
            _grupoManejador = new GrupoManejador();
        }

        private void Calificacion_Load(object sender, EventArgs e)
        {
            buscarcalificacion("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }
        private void buscarcalificacion(string filtro)
        {

            dtgCalificacion.DataSource = _calificacionManejador.GetCalificaciones(filtro);

        }
        private void TraerGrupo(string filtro)
        {
            cmbGrupo.DataSource = _grupoManejador.GetGrupo(filtro);
            cmbGrupo.DisplayMember = "nombregrupo";
            //cmbGrupo.ValueMember = "idgrupo";

        }
        private void TraerMateria(string filtro)
        {
            cmbMateria.DataSource = _asignacionManejador.GetAsignacions(cmbGrupo.Text);
            cmbMateria.DisplayMember = "materiaa";
            //cmbMateria.ValueMember = "idasignacion";

        }
        private void TraerAlumno(string filtro)
        {
            cmbAlumno.DataSource = _grupoAlumnoManejador.GetGrupoAlumnos(cmbGrupo.Text);
            cmbAlumno.DisplayMember = "nombrealumno";
            //cmbAlumno.ValueMember = "idgrupoa";

        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            cmbGrupo.Enabled = activar;
            cmbMateria.Enabled = activar;
            cmbAlumno.Enabled = activar;
            txtParcial1.Enabled = activar;
            txtParcial2.Enabled = activar;
            txtParcial3.Enabled = activar;
            txtParcial4.Enabled = activar;

        }
        private void LimpiarCuadros()
        {
            cmbGrupo.Text = "";
            cmbMateria.Text = "";
            cmbAlumno.Text = "";
            txtParcial1.Text = "";
            txtParcial2.Text = "";
            txtParcial3.Text = "";
            txtParcial4.Text = "";


        }
        private void GuardarCalificaion()
        {
            _calificacionManejador.Guardar(new Calificaciones
            {
                Idparcial1 = Convert.ToInt32(lblId.Text),
                Grupo = cmbGrupo.Text,
                Materia = cmbMateria.Text,
                Nombrealumno = cmbAlumno.Text,
                Parcial1 = Convert.ToInt32( txtParcial1.Text),
                Parcial2 = Convert.ToInt32(txtParcial2.Text),
                Parcial3 = Convert.ToInt32(txtParcial3.Text),
                Parcial4 = Convert.ToInt32(txtParcial4.Text),


            });
        }
        private void EliminarCalificacion()
        {
            var Idcalificacion = dtgCalificacion.CurrentRow.Cells["idparcial1"].Value;
            _calificacionManejador.Eliminar(Convert.ToInt32(Idcalificacion));
        }
        private void ModificarCalificacion()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);

            lblId.Text = dtgCalificacion.CurrentRow.Cells["idparcial1"].Value.ToString();
            cmbGrupo.Text = dtgCalificacion.CurrentRow.Cells["grupo"].Value.ToString();
            cmbMateria.Text = dtgCalificacion.CurrentRow.Cells["materia"].Value.ToString();
            cmbAlumno.Text = dtgCalificacion.CurrentRow.Cells["nombrealumno"].Value.ToString();
            txtParcial1.Text = dtgCalificacion.CurrentRow.Cells["parcial1"].Value.ToString();
            txtParcial2.Text = dtgCalificacion.CurrentRow.Cells["parcial2"].Value.ToString();
            txtParcial3.Text = dtgCalificacion.CurrentRow.Cells["parcial3"].Value.ToString();
            txtParcial4.Text = dtgCalificacion.CurrentRow.Cells["parcial4"].Value.ToString();



        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            buscarcalificacion(txtBuscar.Text);
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
            cmbGrupo.Focus();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            try
            {
                GuardarCalificaion();                
                LimpiarCuadros();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar este registro", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarCalificacion();
                    buscarcalificacion("");

                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }

            }
        }

        private void cmbGrupo_SelectedIndexChanged(object sender, EventArgs e)
        {
            TraerAlumno("");
            TraerMateria("");
        }

        private void cmbMateria_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void cmbAlumno_Click(object sender, EventArgs e)
        {
            
        }

        private void cmbGrupo_Click(object sender, EventArgs e)
        {
            TraerGrupo("");
            
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void dtgCalificacion_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarCalificacion();
                buscarcalificacion("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            ExportarExcel(dtgCalificacion);
        }
        private void ExportarExcel(DataGridView grd)
        {
            try
            {
                SaveFileDialog fichero = new SaveFileDialog();
                fichero.Filter = "Execel(*.xls)|*.xls";
                if (fichero.ShowDialog() == DialogResult.OK)
                {
                    Microsoft.Office.Interop.Excel.Application aplicacion;
                    Workbook libros_trabajo;
                    Worksheet hoja_trabajo;
                    aplicacion = new Microsoft.Office.Interop.Excel.Application();
                    libros_trabajo = aplicacion.Workbooks.Add();
                    hoja_trabajo = (Microsoft.Office.Interop.Excel.Worksheet)libros_trabajo.Worksheets.get_Item(1);

                    for (int i = 0; i < grd.Rows.Count; i++)
                    {
                        for (int j = 0; j < grd.Columns.Count; j++)
                        {
                            hoja_trabajo.Cells[i + 1, j + 1] = grd.Rows[i].Cells[j].Value.ToString();
                        }

                    }
                    libros_trabajo.SaveAs(fichero.FileName,
                        Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal);
                    libros_trabajo.Close(true);
                    aplicacion.Quit();

                    MessageBox.Show("Reporte Terminado", "Reporte", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception)
            {

                MessageBox.Show("Fallo la creacion del archivo intenta de nuevo con otro nombre", "Error de creacion");
            }
        }
    }
}
