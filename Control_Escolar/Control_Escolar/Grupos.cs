﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidad.ControlEscolar;
using LogicaNegocio.ControlEscolar;

namespace Control_Escolar
{
    public partial class Grupos : Form
    {
        private GrupoManejador _grupoManejador;
        public Grupos()
        {
            InitializeComponent();
            _grupoManejador = new GrupoManejador();
        }

        private void Grupo_Load(object sender, EventArgs e)
        {
            buscargrupo("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }
        private void buscargrupo(string filtro)
        {

            dtgGrupo.DataSource = _grupoManejador.GetGrupo(filtro);

        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            txtGrupo.Enabled = activar;
        }
        private void LimpiarCuadros()
        {
            txtGrupo.Text = "";
        }
        private void GuardarGrupo()
        {
            _grupoManejador.Guardar(new Grupo
            {               
                Nombregrupo = txtGrupo.Text,              

            });
        }
        private void EliminarGrupo()
        {
            var Idgrupo = dtgGrupo.CurrentRow.Cells["nombregrupo"].Value;
            _grupoManejador.Eliminar(Idgrupo.ToString());
        }
        private void ModificarGrupo()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);

            
            txtGrupo.Text = dtgGrupo.CurrentRow.Cells["nombregrupo"].Value.ToString();
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            buscargrupo(txtBuscar.Text);
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
            txtGrupo.Focus();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            try
            {
                GuardarGrupo();
                LimpiarCuadros();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar este registro", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarGrupo();
                    buscargrupo("");

                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }

            }
        }
        private void dtgGrupo_DoubleClick(object sender, EventArgs e)
        {
        }
        private void dtgGrupo_CellContextMenuStripNeeded(object sender, DataGridViewCellContextMenuStripNeededEventArgs e)
        {
        }
        private void dtgGrupo_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarGrupo();
                buscargrupo("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
    }
}
