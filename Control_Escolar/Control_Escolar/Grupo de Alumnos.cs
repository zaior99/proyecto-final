﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidad.ControlEscolar;
using LogicaNegocio.ControlEscolar;


namespace Control_Escolar
{
    public partial class Grupo_de_Alumnos : Form
    {
        private GrupoAlumnoManejador _grupoAlumnoManejador;
        private AlumnoManejador _alumnoManejador;
        private GrupoManejador _grupoManejador;
        
        public Grupo_de_Alumnos()
        {
            InitializeComponent();
            _grupoAlumnoManejador = new GrupoAlumnoManejador();
            _alumnoManejador = new AlumnoManejador();
            _grupoManejador = new GrupoManejador();
                     
        }

        private void Grupo_de_Alumnos_Load(object sender, EventArgs e)
        {
            buscargrupoalumno("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }
        private void buscargrupoalumno(string filtro)
        {

            dtgGrupoAlumno.DataSource = _grupoAlumnoManejador.GetGrupoAlumnos(filtro);

        }
        private void TraerAlumno(string filtro)
        {
            cmbAlumno.DataSource = _alumnoManejador.GetAlumno(filtro);
            cmbAlumno.DisplayMember = "nombre";
            cmbAlumno.ValueMember = "idalumno";

        }
        private void TraerGrupo(string filtro)
        {
            cmbGrupo.DataSource = _grupoManejador.GetGrupo(filtro);
            cmbGrupo.DisplayMember = "nombregrupo";
            //cmbGrupo.ValueMember = "idgrupo";

        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            cmbAlumno.Enabled = activar;
            cmbGrupo.Enabled = activar;
            

        }
        private void LimpiarCuadros()
        {
            cmbAlumno.Text = "";
            cmbGrupo.Text = "";
            

        }
        private void GuardarGrupoAlumno()
        {
            _grupoAlumnoManejador.Guardar(new GrupoAlumnos
            {
                Idgrupoa = Convert.ToInt32(lblId.Text),
                Nombrealumno = cmbAlumno.Text,
                Fknombregrupo = cmbGrupo.Text,

            });
        }
        private void EliminarGrupoAlumno()
        {
            var Idgrupoalumno = dtgGrupoAlumno.CurrentRow.Cells["idgrupoa"].Value;
            _grupoAlumnoManejador.Eliminar(Convert.ToInt32(Idgrupoalumno));
        }
        private void ModificarGrupoAlumno()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);

            lblId.Text = dtgGrupoAlumno.CurrentRow.Cells["idgrupoa"].Value.ToString();           
            cmbAlumno.Text = dtgGrupoAlumno.CurrentRow.Cells["nombrealumno"].Value.ToString();
            cmbGrupo.Text = dtgGrupoAlumno.CurrentRow.Cells["grupo"].Value.ToString();


        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            buscargrupoalumno(txtBuscar.Text);
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
            cmbAlumno.Focus();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            try
            {
                GuardarGrupoAlumno();              
                LimpiarCuadros();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar este registro", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarGrupoAlumno();
                    buscargrupoalumno("");
                    
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }

            }
        }

        private void cmbAlumno_Click(object sender, EventArgs e)
        {
            TraerAlumno("");
        }

        private void dtgGrupoAlumno_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarGrupoAlumno();
                buscargrupoalumno("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void cmbAlumno_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbGrupo_Click(object sender, EventArgs e)
        {
            TraerGrupo("");
        }
    }
}
