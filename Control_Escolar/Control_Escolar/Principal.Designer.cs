﻿namespace Control_Escolar
{
    partial class Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.msMenuPrincipal = new System.Windows.Forms.MenuStrip();
            this.catalogosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usuariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alumnoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profesoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.estudioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.materiasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.escuelaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gruposDeAlumnosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grupoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asiganarGrupoAAlumnoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asigcacionDeMaestrosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calificacionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.msMenuPrincipal.SuspendLayout();
            this.SuspendLayout();
            // 
            // msMenuPrincipal
            // 
            this.msMenuPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.catalogosToolStripMenuItem});
            this.msMenuPrincipal.Location = new System.Drawing.Point(0, 0);
            this.msMenuPrincipal.Name = "msMenuPrincipal";
            this.msMenuPrincipal.Size = new System.Drawing.Size(800, 24);
            this.msMenuPrincipal.TabIndex = 1;
            this.msMenuPrincipal.Text = "menuStrip1";
            // 
            // catalogosToolStripMenuItem
            // 
            this.catalogosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.usuariosToolStripMenuItem,
            this.alumnoToolStripMenuItem,
            this.profesoresToolStripMenuItem,
            this.estudioToolStripMenuItem,
            this.materiasToolStripMenuItem,
            this.escuelaToolStripMenuItem,
            this.gruposDeAlumnosToolStripMenuItem,
            this.asigcacionDeMaestrosToolStripMenuItem,
            this.calificacionesToolStripMenuItem});
            this.catalogosToolStripMenuItem.Name = "catalogosToolStripMenuItem";
            this.catalogosToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.catalogosToolStripMenuItem.Text = "Catalogos";
            // 
            // usuariosToolStripMenuItem
            // 
            this.usuariosToolStripMenuItem.BackgroundImage = global::Control_Escolar.Properties.Resources.icons8_user_group_30;
            this.usuariosToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.usuariosToolStripMenuItem.Name = "usuariosToolStripMenuItem";
            this.usuariosToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.usuariosToolStripMenuItem.Text = "Usuario";
            this.usuariosToolStripMenuItem.Click += new System.EventHandler(this.usuariosToolStripMenuItem_Click);
            // 
            // alumnoToolStripMenuItem
            // 
            this.alumnoToolStripMenuItem.BackgroundImage = global::Control_Escolar.Properties.Resources.icons8_user_30;
            this.alumnoToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.alumnoToolStripMenuItem.Name = "alumnoToolStripMenuItem";
            this.alumnoToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.alumnoToolStripMenuItem.Text = "Alumno";
            this.alumnoToolStripMenuItem.Click += new System.EventHandler(this.alumnoToolStripMenuItem_Click);
            // 
            // profesoresToolStripMenuItem
            // 
            this.profesoresToolStripMenuItem.BackgroundImage = global::Control_Escolar.Properties.Resources.icons8_man_teacher;
            this.profesoresToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.profesoresToolStripMenuItem.Name = "profesoresToolStripMenuItem";
            this.profesoresToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.profesoresToolStripMenuItem.Text = "Profesor";
            this.profesoresToolStripMenuItem.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.profesoresToolStripMenuItem.Click += new System.EventHandler(this.profesoresToolStripMenuItem_Click);
            // 
            // estudioToolStripMenuItem
            // 
            this.estudioToolStripMenuItem.BackgroundImage = global::Control_Escolar.Properties.Resources.icons8_product_documents;
            this.estudioToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.estudioToolStripMenuItem.Name = "estudioToolStripMenuItem";
            this.estudioToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.estudioToolStripMenuItem.Text = "Estudio";
            this.estudioToolStripMenuItem.Click += new System.EventHandler(this.estudioToolStripMenuItem_Click);
            // 
            // materiasToolStripMenuItem
            // 
            this.materiasToolStripMenuItem.BackgroundImage = global::Control_Escolar.Properties.Resources.mochila1;
            this.materiasToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.materiasToolStripMenuItem.Name = "materiasToolStripMenuItem";
            this.materiasToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.materiasToolStripMenuItem.Text = "Materia";
            this.materiasToolStripMenuItem.Click += new System.EventHandler(this.materiasToolStripMenuItem_Click);
            // 
            // escuelaToolStripMenuItem
            // 
            this.escuelaToolStripMenuItem.BackgroundImage = global::Control_Escolar.Properties.Resources.school1;
            this.escuelaToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.escuelaToolStripMenuItem.Name = "escuelaToolStripMenuItem";
            this.escuelaToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.escuelaToolStripMenuItem.Text = "Escuela";
            this.escuelaToolStripMenuItem.Click += new System.EventHandler(this.escuelaToolStripMenuItem_Click);
            // 
            // gruposDeAlumnosToolStripMenuItem
            // 
            this.gruposDeAlumnosToolStripMenuItem.BackgroundImage = global::Control_Escolar.Properties.Resources.icons8_user_group;
            this.gruposDeAlumnosToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.gruposDeAlumnosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.grupoToolStripMenuItem,
            this.asiganarGrupoAAlumnoToolStripMenuItem});
            this.gruposDeAlumnosToolStripMenuItem.Name = "gruposDeAlumnosToolStripMenuItem";
            this.gruposDeAlumnosToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.gruposDeAlumnosToolStripMenuItem.Text = "Grupo";
            this.gruposDeAlumnosToolStripMenuItem.Click += new System.EventHandler(this.gruposDeAlumnosToolStripMenuItem_Click);
            // 
            // grupoToolStripMenuItem
            // 
            this.grupoToolStripMenuItem.BackgroundImage = global::Control_Escolar.Properties.Resources.icons8_create_;
            this.grupoToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.grupoToolStripMenuItem.Name = "grupoToolStripMenuItem";
            this.grupoToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.grupoToolStripMenuItem.Text = "Crear Grupo";
            this.grupoToolStripMenuItem.Click += new System.EventHandler(this.grupoToolStripMenuItem_Click);
            // 
            // asiganarGrupoAAlumnoToolStripMenuItem
            // 
            this.asiganarGrupoAAlumnoToolStripMenuItem.BackgroundImage = global::Control_Escolar.Properties.Resources.icons8_group;
            this.asiganarGrupoAAlumnoToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.asiganarGrupoAAlumnoToolStripMenuItem.Name = "asiganarGrupoAAlumnoToolStripMenuItem";
            this.asiganarGrupoAAlumnoToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.asiganarGrupoAAlumnoToolStripMenuItem.Text = "Asignar grupo a Alumno";
            this.asiganarGrupoAAlumnoToolStripMenuItem.Click += new System.EventHandler(this.asiganarGrupoAAlumnoToolStripMenuItem_Click);
            // 
            // asigcacionDeMaestrosToolStripMenuItem
            // 
            this.asigcacionDeMaestrosToolStripMenuItem.BackgroundImage = global::Control_Escolar.Properties.Resources.icons8_class;
            this.asigcacionDeMaestrosToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.asigcacionDeMaestrosToolStripMenuItem.Name = "asigcacionDeMaestrosToolStripMenuItem";
            this.asigcacionDeMaestrosToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.asigcacionDeMaestrosToolStripMenuItem.Text = "Asigcacion de clases";
            this.asigcacionDeMaestrosToolStripMenuItem.Click += new System.EventHandler(this.asigcacionDeMaestrosToolStripMenuItem_Click);
            // 
            // calificacionesToolStripMenuItem
            // 
            this.calificacionesToolStripMenuItem.BackgroundImage = global::Control_Escolar.Properties.Resources.icons8_grades31;
            this.calificacionesToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.calificacionesToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.calificacionesToolStripMenuItem.Name = "calificacionesToolStripMenuItem";
            this.calificacionesToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.calificacionesToolStripMenuItem.Text = "Calificaciones";
            this.calificacionesToolStripMenuItem.Click += new System.EventHandler(this.calificacionesToolStripMenuItem_Click);
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Brown;
            this.BackgroundImage = global::Control_Escolar.Properties.Resources.docs;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.msMenuPrincipal);
            this.DoubleBuffered = true;
            this.Name = "Principal";
            this.Text = "Principal";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.msMenuPrincipal.ResumeLayout(false);
            this.msMenuPrincipal.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip msMenuPrincipal;
        private System.Windows.Forms.ToolStripMenuItem catalogosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usuariosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alumnoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profesoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem estudioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem materiasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem escuelaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gruposDeAlumnosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asigcacionDeMaestrosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calificacionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem grupoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asiganarGrupoAAlumnoToolStripMenuItem;
    }
}