﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidad.ControlEscolar;
using LogicaNegocio.ControlEscolar;

namespace Control_Escolar
{
    public partial class Materia : Form
    {
        private MateriaManejador _materiaManejador;
        public Materia()
        {
            InitializeComponent();
            _materiaManejador = new MateriaManejador();
        }
        private void Materia_Load(object sender, EventArgs e)
        {
            buscarmateria("");
            ControlarBotones(true,false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }
        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }
        private void BtnGuardarMateria_Click(object sender, EventArgs e)
        {

        }
        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            buscarmateria("");
        }
        private void buscarmateria(string filtro)
        {

            dtgMateria.DataSource = _materiaManejador.GetMateria(filtro);
        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            txtMateria.Enabled = activar;
            txtHrsteoria.Enabled = activar;
            txtHrsPractica.Enabled = activar;
            txtCreditos.Enabled = activar;
            
        }
        private void LimpiarCuadros()
        {
            txtMateria.Text = "";
            txtHrsteoria.Text = "";
            txtHrsPractica.Text = "";
            txtCreditos.Text = "";
            
        }
        private void guardarMateria()
        {
            _materiaManejador.Guardar(new Materias
            {
                Idmateria = Convert.ToInt32(lblId.Text),
                Nombremateria = txtMateria.Text,
                Hrsteoria = Convert.ToInt32(txtHrsteoria.Text),
                Hrspractica = Convert.ToInt32(txtHrsPractica.Text),
                Creditos = Convert.ToInt32(txtCreditos.Text),

            });
        }
        private void EliminarMateria()
        {
            var Idmateria = dtgMateria.CurrentRow.Cells["idmateria"].Value;
            _materiaManejador.Eliminar(Convert.ToInt32(Idmateria));
        }
        private void ModificarMateria()
        {
            ControlarCuadros(true);
            ControlarBotones(false,true, true, false);

            lblId.Text = dtgMateria.CurrentRow.Cells["idmateria"].Value.ToString();
            txtMateria.Text = dtgMateria.CurrentRow.Cells["nombremateria"].Value.ToString();
            txtHrsteoria.Text = dtgMateria.CurrentRow.Cells["hrsteoria"].Value.ToString();
            txtHrsPractica.Text = dtgMateria.CurrentRow.Cells["hrspractica"].Value.ToString();
            txtCreditos.Text = dtgMateria.CurrentRow.Cells["creditos"].Value.ToString();
           
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true,false, false, true);
            ControlarCuadros(false);
            try
            {
                guardarMateria();
                LimpiarCuadros();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true,false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar este registro", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarMateria();
                    buscarmateria("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }

            }
        }

        private void dtgMateria_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarMateria();
                buscarmateria("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void txtHrsPractica_Leave(object sender, EventArgs e)
        {
            int t, p, r;
            t = int.Parse(txtHrsteoria.Text);
            p = int.Parse(txtHrsPractica.Text);
            r = t + p;

            txtCreditos.Text = r.ToString();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
            txtMateria.Focus();
        }
    }
}
