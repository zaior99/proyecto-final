﻿namespace Control_Escolar
{
    partial class Estudio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbProfesor = new System.Windows.Forms.ComboBox();
            this.txtEstudios = new System.Windows.Forms.TextBox();
            this.dtgEstudios = new System.Windows.Forms.DataGridView();
            this.txtBuscar = new System.Windows.Forms.TextBox();
            this.lblBuscar = new System.Windows.Forms.Label();
            this.lblId = new System.Windows.Forms.Label();
            this.txtPDF = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnCargarPDF = new System.Windows.Forms.Button();
            this.btnEliminarPDF = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtgEstudios)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(62, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Profesor";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(62, 134);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Estudios";
            // 
            // cmbProfesor
            // 
            this.cmbProfesor.FormattingEnabled = true;
            this.cmbProfesor.Location = new System.Drawing.Point(124, 74);
            this.cmbProfesor.Name = "cmbProfesor";
            this.cmbProfesor.Size = new System.Drawing.Size(165, 25);
            this.cmbProfesor.TabIndex = 2;
            this.cmbProfesor.SelectedIndexChanged += new System.EventHandler(this.cmbProfesor_SelectedIndexChanged);
            this.cmbProfesor.Click += new System.EventHandler(this.cmbProfesor_Click);
            // 
            // txtEstudios
            // 
            this.txtEstudios.Location = new System.Drawing.Point(124, 126);
            this.txtEstudios.Name = "txtEstudios";
            this.txtEstudios.Size = new System.Drawing.Size(165, 25);
            this.txtEstudios.TabIndex = 3;
            this.txtEstudios.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // dtgEstudios
            // 
            this.dtgEstudios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgEstudios.Location = new System.Drawing.Point(21, 257);
            this.dtgEstudios.Name = "dtgEstudios";
            this.dtgEstudios.Size = new System.Drawing.Size(508, 150);
            this.dtgEstudios.TabIndex = 4;
            this.dtgEstudios.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgEstudios_CellDoubleClick);
            // 
            // txtBuscar
            // 
            this.txtBuscar.Location = new System.Drawing.Point(124, 13);
            this.txtBuscar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtBuscar.Name = "txtBuscar";
            this.txtBuscar.Size = new System.Drawing.Size(405, 25);
            this.txtBuscar.TabIndex = 89;
            this.txtBuscar.TextChanged += new System.EventHandler(this.txtBuscar_TextChanged);
            // 
            // lblBuscar
            // 
            this.lblBuscar.AutoSize = true;
            this.lblBuscar.Location = new System.Drawing.Point(63, 21);
            this.lblBuscar.Name = "lblBuscar";
            this.lblBuscar.Size = new System.Drawing.Size(55, 17);
            this.lblBuscar.TabIndex = 90;
            this.lblBuscar.Text = "BUSCAR";
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Font = new System.Drawing.Font("Segoe UI Symbol", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblId.Location = new System.Drawing.Point(588, 10);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(22, 25);
            this.lblId.TabIndex = 106;
            this.lblId.Text = "0";
            // 
            // txtPDF
            // 
            this.txtPDF.Location = new System.Drawing.Point(65, 205);
            this.txtPDF.Name = "txtPDF";
            this.txtPDF.Size = new System.Drawing.Size(261, 25);
            this.txtPDF.TabIndex = 107;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(62, 175);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 17);
            this.label3.TabIndex = 108;
            this.label3.Text = "Documento PDF";
            // 
            // btnCargarPDF
            // 
            this.btnCargarPDF.Location = new System.Drawing.Point(332, 207);
            this.btnCargarPDF.Name = "btnCargarPDF";
            this.btnCargarPDF.Size = new System.Drawing.Size(48, 23);
            this.btnCargarPDF.TabIndex = 110;
            this.btnCargarPDF.Text = "...";
            this.btnCargarPDF.UseVisualStyleBackColor = true;
            this.btnCargarPDF.Click += new System.EventHandler(this.btnCargarPDF_Click);
            // 
            // btnEliminarPDF
            // 
            this.btnEliminarPDF.Location = new System.Drawing.Point(386, 207);
            this.btnEliminarPDF.Name = "btnEliminarPDF";
            this.btnEliminarPDF.Size = new System.Drawing.Size(42, 23);
            this.btnEliminarPDF.TabIndex = 111;
            this.btnEliminarPDF.Text = "x";
            this.btnEliminarPDF.UseVisualStyleBackColor = true;
            this.btnEliminarPDF.Click += new System.EventHandler(this.btnEliminarPDF_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Image = global::Control_Escolar.Properties.Resources.icons8_remove_64;
            this.btnEliminar.Location = new System.Drawing.Point(554, 215);
            this.btnEliminar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(56, 45);
            this.btnEliminar.TabIndex = 105;
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Image = global::Control_Escolar.Properties.Resources.icons8_cancel_64__1_;
            this.btnCancelar.Location = new System.Drawing.Point(554, 159);
            this.btnCancelar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(56, 48);
            this.btnCancelar.TabIndex = 104;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Image = global::Control_Escolar.Properties.Resources.icons8_save_close_40__1_;
            this.btnGuardar.Location = new System.Drawing.Point(554, 108);
            this.btnGuardar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(56, 43);
            this.btnGuardar.TabIndex = 103;
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnNuevo
            // 
            this.btnNuevo.Image = global::Control_Escolar.Properties.Resources.icons8_add_64;
            this.btnNuevo.Location = new System.Drawing.Point(554, 40);
            this.btnNuevo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(56, 46);
            this.btnNuevo.TabIndex = 102;
            this.btnNuevo.UseVisualStyleBackColor = true;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Image = global::Control_Escolar.Properties.Resources.icons8_search_48;
            this.label5.Location = new System.Drawing.Point(18, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 34);
            this.label5.TabIndex = 91;
            this.label5.Text = "\r\n         ";
            // 
            // Estudio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(630, 438);
            this.Controls.Add(this.btnCargarPDF);
            this.Controls.Add(this.btnEliminarPDF);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtPDF);
            this.Controls.Add(this.lblId);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.btnNuevo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtBuscar);
            this.Controls.Add(this.lblBuscar);
            this.Controls.Add(this.dtgEstudios);
            this.Controls.Add(this.txtEstudios);
            this.Controls.Add(this.cmbProfesor);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Estudio";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Estudio";
            this.Load += new System.EventHandler(this.Estudio_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgEstudios)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbProfesor;
        private System.Windows.Forms.TextBox txtEstudios;
        private System.Windows.Forms.DataGridView dtgEstudios;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtBuscar;
        private System.Windows.Forms.Label lblBuscar;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.TextBox txtPDF;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnCargarPDF;
        private System.Windows.Forms.Button btnEliminarPDF;
    }
}